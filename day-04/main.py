def make_list(string):
    list = []
    for number in string.split(" "):
        if not number == "":
            list.append(int(number))
    return list

class Card:
    def __init__(self, winning_numbers, scratch_numbers):
        self.amount = 1
        self.winning_numbers = winning_numbers
        self.scratch_numbers = scratch_numbers

def part_one():
    result = 0
    for line in input:
        winning_numbers = make_list(line.split(": ")[1].split("|")[0])
        scratch_numbers = make_list(line.split("|")[1])
        hits = 0
        for number in scratch_numbers:
            if number in winning_numbers:
                hits *= 2
                if hits == 0:
                    hits = 1
        result += hits
    print("Result of part one:", result)

def part_two():
    result = 0
    cards = []
    for line in input:
        winning_numbers = make_list(line.split(": ")[1].split("|")[0])
        scratch_numbers = make_list(line.split("|")[1])
        cards.append(Card(winning_numbers, scratch_numbers))
    
    for x, card in enumerate(cards):
        hits = 0
        for number in card.scratch_numbers:
            if number in card.winning_numbers:
                hits += 1
        for number in range(hits):
            number+=1
            if x+number < len(cards):
                cards[x+number].amount += card.amount
    for card in cards:
        result += card.amount
    print("Result of part two:", result)

if __name__ == "__main__":
    input = open("./input", "r").readlines()
    part_one()
    part_two()
