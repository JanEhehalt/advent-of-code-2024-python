##############
#   PART 1   #
##############

def part_one():
    result = 0
    for line in input:
        first_digit = ""
        last_digit = ""
        for char in line:
            for number in range(10): 
                if char == str(number): # checking if char is a number between 0 and 9
                    if first_digit == "":
                        first_digit = char
                    last_digit = char
                    break
        result += int(first_digit + last_digit)
    print("Result of part one:", result)

##############
#   PART 2   #
##############

def part_two():
    spelled_numbers = {
        "zero": "0",
        "one": "1",
        "two": "2",
        "three": "3",
        "four": "4",
        "five": "5",
        "six": "6",
        "seven": "7",
        "eight": "8",
        "nine": "9"
    }

    result = 0
    for line in input:
        first_digit = ""
        last_digit = ""
        substring = ""
        for char in line:
            was_number = False
            for number in range(10):
                if char == str(number):
                    if first_digit == "":
                        first_digit = char
                    last_digit = char
                    was_number = True
                    break
            if was_number:
                substring = ""
            else:
                substring += char
            for spelled_number in spelled_numbers.keys():
                if spelled_number in substring:
                    if first_digit == "":
                        first_digit = spelled_numbers[spelled_number]
                    last_digit = spelled_numbers[spelled_number]
                    substring = spelled_number[1:] # "eighthree" has to evaluate to 83! thx to https://www.reddit.com/r/adventofcode/comments/1884fpl/2023_day_1for_those_who_stuck_on_part_2/
                    break
        result += int(first_digit + last_digit)

    print("Result of part two:", result)

if __name__ == "__main__":
    input = open("./input", "r").readlines()
    part_one()
    part_two()
