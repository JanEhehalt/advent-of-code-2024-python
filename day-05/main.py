map_names = ["seed-to-soil", "soil-to-fertilizer", "fertilizer-to-water", "water-to-light", "light-to-temperature", "temperature-to-humidity", "humidity-to-location"]

maps = {}
for i in map_names:
    maps[i] = []

class Mapping:
    def __init__(self, dest_start, src_start, range):
        self.src_start = int(src_start)
        self.dest_start = int(dest_start)
        self.range = int(range)

def generate_map(string, map_name):
    for line in string.split("\n"):
        numbers = line.split(" ")
        if len(numbers) == 1:
            break
        if len(maps[map_name]) == 0:
            maps[map_name].append(Mapping(numbers[0], numbers[1], numbers[2]))
        else:
            x = len(maps[map_name])
            inserted = False
            for mapping in maps[map_name][::-1]:
                if int(mapping.src_start) < int(numbers[1]):
                    maps[map_name].insert(x, Mapping(numbers[0], numbers[1], numbers[2]))
                    inserted = True
                    break
                x -= 1
            if not inserted:
                maps[map_name].insert(0, Mapping(numbers[0], numbers[1], numbers[2]))

def traverse_maps(seed):
    search_id = seed
    for map_name in map_names:
        for mapping in maps[map_name]:
            if search_id > mapping.src_start and search_id < mapping.src_start + mapping.range:
                search_id = mapping.dest_start + search_id - mapping.src_start
                break
    return search_id

def part_one():
    seeds = [int(x) for x in input.split("seeds: ")[1].split("\n")[0].split(" ")]
    results = []
    for seed in seeds:
        results.append(traverse_maps(seed))
    # find smallest result
    result = results[0]
    for n in results:
        if n < result:
            result = n
    print("Result of part one:", result)

def part_two():
    print("Part two does not work yet because it is far too inefficient")
    return
    result = 0
    seeds = [int(x) for x in input.split("seeds: ")[1].split("\n")[0].split(" ")]

    result = traverse_maps(int(seeds[0]))
    for x, seed_src in enumerate(seeds):
        if x % 2 == 0:
            for ran in range(seeds[x+1]):
                result = min(result, traverse_maps(seed_src + ran))

    print("Result of part two:", result)

if __name__ == "__main__":
    input = open("./input", "r").read()
    
    for i in range(len(map_names)):
        if i+1 == len(map_names):
            generate_map(input.split(map_names[i]+" map:\n")[1], map_names[i])
        else:
            generate_map(input.split(map_names[i]+" map:\n")[1].split(map_names[i+1])[0], map_names[i])

    part_one()
    part_two()
