def is_number(char):
    for number in range(10):
        if char == str(number):
            return True
    return False

def get_left_number(x_pointer, y_pointer):
    left_number = ""
    while x_pointer >= 0:
        x_pointer -= 1
        if not is_number(input[y_pointer][x_pointer]):
            break
        left_number = input[y_pointer][x_pointer] + left_number
    return "" if left_number == "" else left_number

def get_right_number(x_pointer, y_pointer):
    right_number = ""
    while x_pointer < len(input[0]):
        x_pointer += 1
        if not is_number(input[y_pointer][x_pointer]):
            break
        right_number += input[y_pointer][x_pointer]
    return "" if right_number == "" else right_number

def get_top_number(x_pointer, y_pointer):
    if y_pointer == 0:
        return []
    y_pointer -= 1
    if is_number(input[y_pointer][x_pointer]):
        return [get_left_number(x_pointer, y_pointer) + input[y_pointer][x_pointer] + get_right_number(x_pointer, y_pointer)]
    else:
        return [get_left_number(x_pointer, y_pointer), get_right_number(x_pointer, y_pointer)]
        
def get_bottom_number(x_pointer, y_pointer):
    if y_pointer+1 == len(input):
        return []
    y_pointer += 1
    if is_number(input[y_pointer][x_pointer]):
        return [get_left_number(x_pointer, y_pointer) + input[y_pointer][x_pointer] + get_right_number(x_pointer, y_pointer)]
    else:
        return [get_left_number(x_pointer, y_pointer), get_right_number(x_pointer, y_pointer)]

def part_one():
    result = 0
    for y, line in enumerate(input):
        line = line.replace("\n", "")
        for x, char in enumerate(line):
            if not char == "." and not is_number(char):
                left = get_left_number(x,y)
                result += int(left) if not left == "" else 0
                right = get_right_number(x,y)
                result += int(right) if not right == "" else 0
                for number in get_top_number(x,y):
                    result += int(number) if not number == "" else 0
                for number in get_bottom_number(x,y):
                    result += int(number) if not number == "" else 0
    print("Result of part one:", result)


def part_two():
    result = 0
    for y, line in enumerate(input):
        line = line.replace("\n", "")
        for x, char in enumerate(line):
            if not char == "." and not is_number(char):
                numbers = []
                left = get_left_number(x,y)
                if not left == "":
                    numbers.append(left)
                right = get_right_number(x,y)
                if not right == "":
                    numbers.append(right)
                for number in get_top_number(x,y):
                    if not number == "":
                        numbers.append(number)
                for number in get_bottom_number(x,y):
                    if not number == "":
                        numbers.append(number)
                if len(numbers) == 2:
                    result += int(numbers[0]) * int(numbers[1])
    print("Result of part two:", result)

if __name__ == "__main__":
    input = open("./input", "r").readlines()
    part_one()
    part_two()

