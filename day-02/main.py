# Returns true/false if input part is possible
# input can be 14blue, 20red or 1green ...
def part_one_helper(part): 
    available_cubes = { "red": 12, "green": 13, "blue": 14 }
    for cube_color in available_cubes: 
        if cube_color in part:
            part = part.split(cube_color)[0]
            if int(part) > available_cubes[cube_color]:
                return False
    return True

def part_one():
    result = 0
    for line in input:
        id = int(line.split("Game ")[1].split(":")[0])
        line = line.split(": ")[1]
        line = line.replace(",", ";")
        line = line.replace(" ", "")
        line = line.replace("\n", "")
        line_possible = True
        for part in line.split(";"):
            if not part_one_helper(part):
                line_possible = False
        if line_possible:
            result += id
    print("Result of part one:", result)

# returns the parts color and number
# input can be 14blue, 20red or 1green ...
def part_two_helper(part): 
    available_cubes = ["red", "green", "blue"]
    for cube_color in available_cubes: 
        if cube_color in part:
            part = part.split(cube_color)[0]
            return [cube_color, int(part)]
    return 0

def part_two():
    result = 0
    for line in input:
        id = int(line.split("Game ")[1].split(":")[0])

        # formatting the line from "Game 1: 3green; 2blue; 1red\n" to "3green;2blue;1red"
        line = line.split(": ")[1]
        line = line.replace(",", ";")
        line = line.replace(" ", "")
        line = line.replace("\n", "")
        
        biggest = {"red": 0, "green": 0, "blue": 0}

        for part in line.split(";"):
            helper_result = part_two_helper(part)
            biggest[helper_result[0]] = max(biggest[helper_result[0]], helper_result[1])
        result += biggest["red"] * biggest["blue"] * biggest["green"]

    print("Result of part two:", result)


if __name__ == "__main__":
    input = open("./input", "r").readlines()
    part_one()
    part_two()